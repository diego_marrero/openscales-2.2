package org.openscales.core.utils
{
	import mx.utils.StringUtil;
	
	import org.openscales.core.Map;
	import org.openscales.geometry.basetypes.Unit;
	import org.openscales.proj4as.ProjProjection;

	// ---------------------------------------------------------------
	//	Utilidades añadidas por BlueGRID a OpenScales
	// ---------------------------------------------------------------
	
	public class BlueGridUtils
	{
		// Rangos a tener en cuenta para el refresco de datos de nuestros WFS
		private	static	var	iScaleRanges		:Array = [500000, 1000000, 2500000];
		
		// Nuestros servidores de mapas
		private static	var iBlueGridServers	:Array = ['188.165.251.207'];

		
		public function BlueGridUtils()
		{
			
		}
		
		public static function getNumericScale (aMap:Map):Number
		{
			var projection			:ProjProjection = ProjProjection.getProjProjection (aMap.projection);
			var resolutionAtCenter	:Number = Unit.getResolutionOnCenter (aMap.resolution.value, aMap.center, projection);
			var unit				:String = ProjProjection.getProjProjection (aMap.projection).projParams.units;
			
			if (projection.projName  == "longlat")
				unit = Unit.METER;
			
			return Math.round (Unit.getScaleDenominatorFromResolution (resolutionAtCenter, unit));
		}
		
		public static function getScaleRange (aMap:Map):String
		{
			var	lCurrentScale	:Number = getNumericScale (aMap);

			for (var i:int = 0; i < iScaleRanges.length; i++){
				if (lCurrentScale <= iScaleRanges [i]){
					return String (i);
				}
			}
			
			return String (i);
		}
		
		public static function thisServerIsMine (aServer:String):Boolean
		{
			for each (var lServer:String in iBlueGridServers){
				if (StringUtil.trim (aServer).indexOf (String (lServer)) != -1){
					return true;
				}
			}
			
			return false;
		}
	}
}

