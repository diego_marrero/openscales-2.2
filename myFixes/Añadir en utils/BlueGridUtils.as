package org.openscales.core.utils
{
	import org.openscales.core.Map;
	import org.openscales.geometry.basetypes.Unit;
	import org.openscales.proj4as.ProjProjection;

	public class BlueGridUtils
	{
		public function BlueGridUtils()
		{
			// Utilidades añadidas por BlueGRID a OpenScales
		}
		
		public static function getNumericScale (aMap:Map):Number
		{
			var projection			:ProjProjection = ProjProjection.getProjProjection (aMap.projection);
			var resolutionAtCenter	:Number = Unit.getResolutionOnCenter (aMap.resolution.value, aMap.center, projection);
			var unit				:String = ProjProjection.getProjProjection (aMap.projection).projParams.units;
			
			if (projection.projName  == "longlat")
				unit = Unit.METER;
			
			return Math.round (Unit.getScaleDenominatorFromResolution (resolutionAtCenter, unit));
		}
		
	}
}

